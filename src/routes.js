import AllCategories from "./views/allCategories";
import AddCategory from "./views/addCategory";
import AllItems from "./views/allItems";
import AddItem from "./views/addItem";

var routes = [
  {
    path: "/allCategories",
    name: "All Categories",
    icon: "ni ni-bullet-list-67 text-red",
    component: AllCategories,
    layout: "/inventory",
  },
  {
    path: "/addCategory",
    name: "ADD Category",
    icon: "ni ni-fat-add text-blue",
    component: AddCategory,
    layout: "/inventory",
  },
  {
    path: "/allItems",
    name: "All Items",
    icon: "ni ni-bullet-list-67 text-red",
    component: AllItems,
    layout: "/inventory",
  },
  {
    path: "/addItems",
    name: "ADD Item",
    icon: "ni ni-fat-add text-blue",
    component: AddItem,
    layout: "/inventory",
  },
];
export default routes;
