
import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
// reactstrap components
import { Container } from "reactstrap";
// core components
import Navbar from "../components/Navbars/Navbar";
import Footer from "../components/Footer/Footer";
import Sidebar from "../components/Sidebar/Sidebar";

import routes from "../routes";

//Logo Import 
import  Logo from '../assets/img/brand/argon-react.png'

class Inventory extends React.Component {
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/inventory") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  render() {
    return (
      <>
      {/* sidebar layout */}
        <Sidebar
          {...this.props}
          routes={routes}
          logo={{
            innerLink: "/inventory/allCategories",
            imgAlt: "Inventory",
            imgSrc : Logo
          }}
        />
        {/* navbar layout */}
        <div className="main-content" ref="mainContent">
          <Navbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
          />
          <Switch>
            {this.getRoutes(routes)}
            <Redirect from="*" to="/inventory/allCategories" />
          </Switch>
          {/* container layout */}
          <Container fluid>
            {/* footer layout */}
            <Footer />
          </Container>
        </div>
      </>
    );
  }
}

export default Inventory;
