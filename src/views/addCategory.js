import React, { Component } from "react";
import Header from "../components/Headers/Header";
//core Component
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col,
  Label,
  Container
} from "reactstrap";

class AddCategory extends Component {
  state = {
    name: "",
    description: "",
    status: "active",
    submitted: false,
  };
  //function for setting Value of Input Feild
  setValue = (e) => {
    const { name, description } = this.state;
    this.setState({ [e.target.name]: e.target.value });
    if (name !== "" && description !== "") {
      this.setState({ submitted: true });
    }
  };
  //function for adding Category
  addCategory = () => {
    const { name, description, status } = this.state;
    if (name !== "" && description !== "") {
      //for add new category to the database
      fetch("http://localhost:1337/category/create", {
        method: "POST",
        body: JSON.stringify({
          name: name,
          description: description,
          status: status,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          console.log(data);
          //Redirect to List of Categories Page
          window.location.href = "/inventory/allCategories";
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  CancleAdd = () => {
    window.location.href = "/inventory/allCategories";
  };
  render() {
    return (
      <>
        <Header />
        <Container className="mt--7" fluid style={{ height: "100%" }}>
          <Col
            lg="5"
            md="7"
            className="header bg-gradient-info pb-8 pt-5 pt-md-8 center mt-2"
          >
            <Card className="bg-secondary shadow border-0">
              <CardHeader className="bg-transparent pb-5">
                <div className="text-muted text-center mt-2 mb-3">
                  <small>Add New Category</small>
                  <Button
                    className="my-4 ml-9"
                    color="gray"
                    type="submit"
                    onClick={this.CancleAdd}
                  >
                    <i className="ni ni-fat-remove" />
                  </Button>
                </div>
              </CardHeader>
              <CardBody className="px-lg-5 py-lg-5">
                <Form role="form">
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-hat-3" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Category Name"
                        type="text"
                        autoComplete="name"
                        name="name"
                        onChange={(e) => this.setValue(e)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-align-left-2" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Category Description"
                        type="text"
                        autoComplete="description"
                        name="description"
                        onChange={(e) => this.setValue(e)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-ui-04" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="radio"
                        id="active"
                        name="status"
                        value="active"
                        autoComplete="satus"
                        className="ml-6 mt-2"
                        onChange={(e) => this.setValue(e)}
                        defaultChecked
                      />
                      <Label for="active" className="ml-5">
                        active
                      </Label>
                      <Input
                        type="radio"
                        id="inactive"
                        name="status"
                        value="inactive"
                        autoComplete="satus"
                        className="ml-9 mt-2"
                        onChange={(e) => this.setValue(e)}
                      />
                      <Label for="inactive" className="ml-5">
                        InActive
                      </Label>
                    </InputGroup>
                  </FormGroup>
                  {/* if value is not empty then Add Button Will Show */}
                  {this.state.submitted ? (
                    <div className="text-center">
                      <Button
                        className="my-4"
                        color="primary"
                        type="submit"
                        onClick={this.addCategory}
                      >
                        Add
                      </Button>
                    </div>
                  ) : (
                    /* if value is empty then Show below text*/
                    "For Adding Data, First You have to enter it!"
                  )}
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Container>
      </>
    );
  }
}

export default AddCategory;
