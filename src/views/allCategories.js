import React, { Component } from "react";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  Media,
  Table,
  Container,
  Row,
  NavItem,
  NavLink,
  Nav,
  Button,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col,
  Label,
} from "reactstrap";
import classnames from "classnames";

// core components
import Header from "../components/Headers/Header";

//All Category Page View

class AllCategories extends Component {
  state = {
    categories: [],
    id: "",
    name: "",
    description: "",
    status: "active",
    submitted: false,
    showEditForm: false,
  };
  //for getting all categories
  componentDidMount() {
    fetch("http://localhost:1337/category/allcategory")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        this.setState({ categories: data.categories });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  //for Deleting Category
  deleteCategory = (id) => {
    fetch("http://localhost:1337/category/delete/" + id, {
      method: "DELETE",
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {})
      .catch((err) => {
        console.log(err);
      });
  };
  //for Editing Category
  EditCategory = (categoryData) => {
    this.setState({
      showEditForm: true,
      id: categoryData.id,
      name: categoryData.name,
      description: categoryData.description,
      status: categoryData.status,
    });
  };
  //function for setting Value of Input Feild
  setValue = (e) => {
    const { name, description } = this.state;
    this.setState({ [e.target.name]: e.target.value });
    if (name !== "" && description !== "") {
      this.setState({ submitted: true });
    }
  };
  //function for adding Category
  EditCategorySubmit = () => {
    const { name, description, status, id } = this.state;
    if (id !== "" && name !== "" && description !== "") {
      fetch("http://localhost:1337/category/update/" + id, {
        method: "PUT",
        body: JSON.stringify({
          name: name,
          description: description,
          status: status,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          console.log(data);
          this.setState({ showEditForm: false, submitted: false });
          //Redirect to List of Categories Page
          window.location.href = "/inventory/allCategories";
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  //close Model
  CancleAdd = () => {
    this.setState({ showEditForm: false, submitted: false });
  };
  render() {
    const {
      categories,
      showEditForm,
      name,
      description,
      status,
    } = this.state;
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid style={{ height: "100%" }}>
          {/* Table */}
          {/* if editted true then show the form for editing */}
          {showEditForm ? (
            <Col
              lg="5"
              md="7"
              className="header bg-gradient-info pb-8 pt-5 pt-md-8 center mt-2"
            >
              <Card className="bg-secondary shadow border-0">
                <CardHeader className="bg-transparent pb-5">
                  <div className="text-muted text-center mt-2 mb-3">
                    <small>Edit Category</small>
                    <Button
                      className="my-4 ml-9"
                      color="gray"
                      type="submit"
                      onClick={this.CancleAdd}
                    >
                      <i className="ni ni-fat-remove" />
                    </Button>
                  </div>
                </CardHeader>
                <CardBody className="px-lg-5 py-lg-5">
                  <Form role="form">
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-hat-3" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Category Name"
                          type="text"
                          autoComplete="name"
                          name="name"
                          value={name}
                          onChange={(e) => this.setValue(e)}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-align-left-2" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Category Description"
                          type="text"
                          autoComplete="description"
                          name="description"
                          value={description}
                          onChange={(e) => this.setValue(e)}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-ui-04" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="radio"
                          id="active"
                          name="status"
                          value="active"
                          autoComplete="satus"
                          className="ml-6 mt-2"
                          onChange={(e) => this.setValue(e)}
                          defaultChecked={status === "active"}
                        />
                        <Label for="active" className="ml-5">
                          active
                        </Label>
                        <Input
                          type="radio"
                          id="inactive"
                          name="status"
                          value="inactive"
                          autoComplete="satus"
                          className="ml-9 mt-2"
                          onChange={(e) => this.setValue(e)}
                          defaultChecked={status === "inactive"}
                        />
                        <Label for="inactive" className="ml-5">
                          InActive
                        </Label>
                      </InputGroup>
                    </FormGroup>
                    {this.state.submitted ? (
                      <div className="text-center">
                        <Button
                          className="my-4"
                          color="primary"
                          type="submit"
                          onClick={this.EditCategorySubmit}
                        >
                          Edit
                        </Button>
                      </div>
                    ) : (
                      "For Editng Data, First You have to Edit it!"
                    )}
                  </Form>
                </CardBody>
              </Card>
            </Col>
          ) : (
            /* Dark table */
            <Row className="mt-5">
              <div className="col">
                <Card className="bg-default shadow">
                  <CardHeader className="bg-transparent border-0">
                    <h3 className="text-white mb-0">Card tables</h3>
                  </CardHeader>
                  <Table
                    className="align-items-center table-dark table-flush"
                    responsive
                  >
                    <thead className="thead-dark">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody>
                      {categories
                        ? categories.map((category) => {
                            return (
                              <tr key={category.id}>
                                <th scope="row">
                                  <Media className="align-items-center">
                                    <span className="mb-0 text-sm">
                                      {category.id}
                                    </span>
                                  </Media>
                                </th>
                                <td>{category.name}</td>
                                <td>{category.description}</td>
                                <td>
                                  <Badge color="" className="badge-dot mr-4">
                                    <i
                                      className={
                                        category.status === "active"
                                          ? "bg-success"
                                          : "bg-warning"
                                      }
                                    />
                                    {category.status}
                                  </Badge>
                                </td>
                                <td>
                                  <div className="d-flex align-items-center">
                                    <div className="col">
                                      <Nav
                                        className="justify-content-start"
                                        pills
                                      >
                                        <NavItem>
                                          <NavLink
                                            className={classnames(
                                              "py-2 px-3  bg-success",
                                              {
                                                active:
                                                  this.state.activeNav === 1,
                                              }
                                            )}
                                            style={{ color: "white" }}
                                            href="#"
                                            onClick={() =>
                                              this.EditCategory(category)
                                            }
                                          >
                                            <span className="d-none d-md-block">
                                              Edit
                                            </span>
                                            <span className="d-md-none">M</span>
                                          </NavLink>
                                        </NavItem>
                                        <NavItem>
                                          <NavLink
                                            className={classnames(
                                              "py-2 px-3  bg-danger",
                                              {
                                                active:
                                                  this.state.activeNav === 2,
                                              }
                                            )}
                                            style={{ color: "white" }}
                                            data-toggle="tab"
                                            href="/inventory/allCategories"
                                            onClick={() =>
                                              this.deleteCategory(category.id)
                                            }
                                          >
                                            <span className="d-none d-md-block">
                                              Delete
                                            </span>
                                            <span className="d-md-none">W</span>
                                          </NavLink>
                                        </NavItem>
                                      </Nav>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            );
                          })
                        : null}
                    </tbody>
                  </Table>
                </Card>
              </div>
            </Row>
          )}
        </Container>
      </>
    );
  }
}

export default AllCategories;
