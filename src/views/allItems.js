import React, { Component } from "react";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  Media,
  Table,
  Container,
  Row,
  NavItem,
  NavLink,
  Nav,
  Button,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col,
  Label,
  CustomInput,
} from "reactstrap";
import classnames from "classnames";

// core components
import Header from "../components/Headers/Header";

//page for All Items View
class AllItems extends Component {
  state = {
    items: [],
    name: "",
    description: "",
    status: "active",
    category: "",
    price: 0,
    submitted: false,
    categories: [],
    showEditForm: false,
  };
  componentDidMount() {
    //for getting All Items from API
    fetch("http://localhost:1337/item/allItems")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ items: data.items });
      })
      .catch((err) => {
        console.log(err);
      });
    //for getting all Categories from API
    fetch("http://localhost:1337/category/allcategory")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ categories: data.categories });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  //for delteting Item
  deleteItem = (id) => {
    fetch("http://localhost:1337/item/delete/" + id, {
      method: "DELETE",
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {})
      .catch((err) => {
        console.log(err);
      });
  };
  //for Editing Category
  EditItem = (ItemData) => {
    this.setState({
      showEditForm: true,
      id: ItemData.id,
      name: ItemData.name,
      description: ItemData.description,
      status: ItemData.status,
      price: ItemData.price,
      category: ItemData.category,
    });
  };
  //function for setting Value of Input Feild
  setValue = (e) => {
    const { name, description, category, price } = this.state;
    console.log(e.target.name, e.target.value);
    this.setState({ [e.target.name]: e.target.value });
    if ((name !== "" && description !== "" && category !== "", price > 0)) {
      this.setState({ submitted: true });
    }
  };
  //function for adding Category
  EditItemSubmit = () => {
    const { name, description, status, id, category, price } = this.state;
    if (id !== "" && name !== "" && description !== "") {
      fetch("http://localhost:1337/item/update/" + id, {
        method: "PUT",
        body: JSON.stringify({
          name: name,
          description: description,
          status: status,
          category: category,
          price: price,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          console.log(data);
          this.setState({ showEditForm: false, submitted: false });
          //Redirect to List of Categories Page
          window.location.href = "/inventory/allCategories";
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  //close Model
  CancleAdd = () => {
    this.setState({ showEditForm: false, submitted: false });
  };
  render() {
    const {
      items,
      showEditForm,
      name,
      description,
      status,
      submitted,
      price,
      category,
    } = this.state;
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid style={{ height: "100%" }}>
          {/* if editted true the show editing form */}
          {showEditForm ? (
            <Col
              lg="5"
              md="7"
              className="header bg-gradient-info pb-8 pt-5 pt-md-8 center mt-2"
            >
              <Card className="bg-secondary shadow border-0">
                <CardHeader className="bg-transparent pb-5">
                  <div className="text-muted text-center mt-2 mb-3">
                    <small>Edit Item</small>
                    <Button
                      className="my-4 ml-9"
                      color="gray"
                      type="submit"
                      onClick={this.CancleAdd}
                    >
                      <i className="ni ni-fat-remove" />
                    </Button>
                  </div>
                </CardHeader>
                <CardBody className="px-lg-5 py-lg-5">
                  <Form role="form">
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-hat-3" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Item Name"
                          type="text"
                          name="name"
                          value={name}
                          onChange={(e) => this.setValue(e)}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-align-left-2" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Item Description"
                          type="text"
                          name="description"
                          value={description}
                          onChange={(e) => this.setValue(e)}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-money-coins" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Item price"
                          type="number"
                          name="price"
                          min="0"
                          value={price}
                          onChange={(e) => this.setValue(e)}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-money-coins" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <CustomInput
                          placeholder="Select Category"
                          type="select"
                          name="category"
                          id="category"
                          value={category}
                          onChange={(e) => this.setValue(e)}
                        >
                          {this.state.categories
                            ? this.state.categories.map((category) => {
                                return (
                                  <option
                                    value={category.name}
                                    key={category.id}
                                  >
                                    {category.name}
                                  </option>
                                );
                              })
                            : null}
                        </CustomInput>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-ui-04" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="radio"
                          id="active"
                          name="status"
                          value="active"
                          autoComplete="satus"
                          className="ml-6 mt-2"
                          onChange={(e) => this.setValue(e)}
                          defaultChecked={status === "active"}
                        />
                        <Label for="active" className="ml-5">
                          active
                        </Label>
                        <Input
                          type="radio"
                          id="inactive"
                          name="status"
                          value="inactive"
                          autoComplete="satus"
                          className="ml-9 mt-2"
                          onChange={(e) => this.setValue(e)}
                          defaultChecked={status === "inactive"}
                        />
                        <Label for="inactive" className="ml-5">
                          InActive
                        </Label>
                      </InputGroup>
                    </FormGroup>
                    {submitted ? (
                      <div className="text-center">
                        <Button
                          className="my-4"
                          color="primary"
                          type="submit"
                          onClick={this.EditItemSubmit}
                        >
                          Edit
                        </Button>
                      </div>
                    ) : (
                      "For Editing Data, First You have to Edit it!"
                    )}
                  </Form>
                </CardBody>
              </Card>
            </Col>
          ) : (
            /* Table */
            /* Dark table */
            <Row className="mt-5">
              <div className="col">
                <Card className="bg-default shadow">
                  <CardHeader className="bg-transparent border-0">
                    <h3 className="text-white mb-0">Card tables</h3>
                  </CardHeader>
                  <Table
                    className="align-items-center table-dark table-flush"
                    responsive
                  >
                    <thead className="thead-dark">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Status</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Actions</th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody>
                      {items
                        ? items.map((item) => {
                            return (
                              <tr key={item.id}>
                                <th scope="row">
                                  <Media className="align-items-center">
                                    <span className="mb-0 text-sm">
                                      {item.id}
                                    </span>
                                  </Media>
                                </th>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>
                                  <Badge color="" className="badge-dot mr-4">
                                    <i
                                      className={
                                        item.status === "active"
                                          ? "bg-success"
                                          : "bg-warning"
                                      }
                                    />
                                    {item.status}
                                  </Badge>
                                </td>
                                <td>{item.category}</td>
                                <td>{item.price}</td>
                                <td>
                                  <div className="d-flex align-items-center">
                                    <div className="col">
                                      <Nav
                                        className="justify-content-start"
                                        pills
                                      >
                                        <NavItem>
                                          <NavLink
                                            className={classnames(
                                              "py-2 px-3  bg-success",
                                              {
                                                active:
                                                  this.state.activeNav === 1,
                                              }
                                            )}
                                            style={{ color: "white" }}
                                            href="#"
                                            onClick={() => this.EditItem(item)}
                                          >
                                            <span className="d-none d-md-block">
                                              Edit
                                            </span>
                                            <span className="d-md-none">M</span>
                                          </NavLink>
                                        </NavItem>
                                        <NavItem>
                                          <NavLink
                                            className={classnames(
                                              "py-2 px-3  bg-danger",
                                              {
                                                active:
                                                  this.state.activeNav === 2,
                                              }
                                            )}
                                            style={{ color: "white" }}
                                            data-toggle="tab"
                                            href="/inventory/allItems"
                                            onClick={() =>
                                              this.deleteItem(item.id)
                                            }
                                          >
                                            <span className="d-none d-md-block">
                                              Delete
                                            </span>
                                            <span className="d-md-none">W</span>
                                          </NavLink>
                                        </NavItem>
                                      </Nav>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            );
                          })
                        : null}
                    </tbody>
                  </Table>
                </Card>
              </div>
            </Row>
          )}
        </Container>
      </>
    );
  }
}

export default AllItems;
