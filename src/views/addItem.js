import React, { Component } from "react";
import Header from "../components/Headers/Header";
//core component
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col,
  Label,
  CustomInput,
  Container
} from "reactstrap";

class AddItem extends Component {
  state = {
    name: "",
    description: "",
    status: "active",
    category: "",
    price: 0,
    submitted: false,
    categories: [],
  };
  //for get categories
  componentDidMount() {
    fetch("http://localhost:1337/category/allcategory")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ categories: data.categories });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  //function for setting Value of Input Feild
  setValue = (e) => {
    const { name, description, category, price } = this.state;
    console.log(e.target.name, e.target.value);
    this.setState({ [e.target.name]: e.target.value });
    if ((name !== "" && description !== "" && category !== "", price > 0)) {
      this.setState({ submitted: true });
    }
  };
  //function for adding Item
  addItem = () => {
    window.location.href = "/inventory/allItems";
    const { name, description, status, category, price } = this.state;
    if (name !== "" && description !== "" && category !== "" && price !== 0) {
      //for add new item to the Database
      fetch("http://localhost:1337/item/create", {
        method: "POST",
        body: JSON.stringify({
          name: name,
          description: description,
          status: status,
          category: category,
          price: price,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          //Redirect to List of Items Page
          // window.location.href = "/inventory/allItems";
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  CancleAdd = () => {
    window.location.href = "/inventory/allItems";
  };
  render() {
    return (
      <>
        <Header />
        <Container className="mt--7" fluid style={{ height: "100%" }}>
          <Col
            lg="5"
            md="7"
            className="header bg-gradient-info pb-8 pt-5 pt-md-8 center"
          >
            <Card className="bg-secondary shadow border-0">
              <CardHeader className="bg-transparent pb-5">
                <div className="text-muted text-center mt-2 mb-3">
                  <small>Add New Item</small>
                  <Button
                    className="my-4 ml-9"
                    color="gray"
                    type="submit"
                    onClick={this.CancleAdd}
                  >
                    <i className="ni ni-fat-remove" />
                  </Button>
                </div>
              </CardHeader>
              <CardBody className="px-lg-5 py-lg-5">
                <Form role="form">
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-hat-3" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Item Name"
                        type="text"
                        name="name"
                        onChange={(e) => this.setValue(e)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-align-left-2" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Item Description"
                        type="text"
                        name="description"
                        onChange={(e) => this.setValue(e)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-money-coins" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Item price"
                        type="number"
                        name="price"
                        min="0"
                        onChange={(e) => this.setValue(e)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-money-coins" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <CustomInput
                        placeholder="Select Category"
                        type="select"
                        name="category"
                        id="category"
                        onChange={(e) => this.setValue(e)}
                      >
                        {this.state.categories
                          ? this.state.categories.map((category) => {
                              return (
                                <option value={category.name} key={category.id}>
                                  {category.name}
                                </option>
                              );
                            })
                          : null}
                      </CustomInput>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-ui-04" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="radio"
                        id="active"
                        name="status"
                        value="active"
                        autoComplete="satus"
                        className="ml-6 mt-2"
                        onChange={(e) => this.setValue(e)}
                        defaultChecked
                      />
                      <Label for="active" className="ml-5">
                        active
                      </Label>
                      <Input
                        type="radio"
                        id="inactive"
                        name="status"
                        value="inactive"
                        autoComplete="satus"
                        className="ml-9 mt-2"
                        onChange={(e) => this.setValue(e)}
                      />
                      <Label for="inactive" className="ml-5">
                        InActive
                      </Label>
                    </InputGroup>
                  </FormGroup>
                  {/* if value is not empty then Add Button Will Show */}
                  {this.state.submitted ? (
                    <div className="text-center">
                      <Button
                        className="my-4"
                        color="primary"
                        type="submit"
                        onClick={this.addItem}
                      >
                        Add
                      </Button>
                    </div>
                  ) : (
                    /* if value is  empty then Show this text */
                    "For Adding Data, First You have to enter it!"
                  )}
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Container>
      </>
    );
  }
}

export default AddItem;
